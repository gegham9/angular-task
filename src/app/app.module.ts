import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoresComponent } from './components/stores/stores.component';
import { StoresFormComponent } from './components/storesForm/storesForm.component';
import { SingleStoreComponent } from './components/single-store/single-store.component';
import { MaterialModule } from './material.module';

const appRoutes: Routes = [
  { path: 'stores', component: StoresComponent },
  { path: 'stores/:id', component: SingleStoreComponent },
  { path: '',
    redirectTo: '/stores',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    StoresComponent,
    StoresFormComponent,
    SingleStoreComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    MaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
