import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

import { StoresService } from '../../services/stores/stores.service';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {
  title: string = "Stores";
  stores: Store[];
  formModel: Store;

  constructor(private storesService: StoresService) {
    this.formModel = new Store("", "", "", "");
  }

  onDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.stores, event.previousIndex, event.currentIndex);
  }

  private getStores(): void {
    this.storesService.getStores()
    .subscribe(stores => this.stores = stores);
  }

  handleEdit(storeId) {
    this.storesService.getStore(storeId)
    .subscribe(store => {
      this.formModel = new Store(store.id, store.name, store.workingTimes, store.address);
    });
  }

  handleFormSubmit = (store) => {
    this.storesService.add(store);
    this.formModel = new Store("", "", "", "");
  }

  ngOnInit() {
    this.getStores();
  }

}

export class Store {
  constructor(
    public id: string,
    public name: string,
    public workingTimes: string,
    public address: string
  ) {}
}