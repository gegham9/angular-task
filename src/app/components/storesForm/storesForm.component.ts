import { Component, Input } from '@angular/core';
import { Store } from '../stores/stores.component';

@Component({
  selector: 'app-storesForm',
  templateUrl: './storesForm.component.html',
  styleUrls: ['./storesForm.component.css']
})
export class StoresFormComponent {
  @Input() onFormSubmit: Function;
  @Input() model: Store;

  constructor() { }

  onSubmit = () => {
    this.onFormSubmit(this.model);
  }

}
