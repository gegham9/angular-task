import { Component, OnInit, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

import { ProductsService, Product } from '../../services/products/products.service';

@Component({
  selector: 'app-single-store',
  templateUrl: './single-store.component.html',
  styleUrls: ['./single-store.component.css']
})
export class SingleStoreComponent implements OnInit {
  storeId: string;
  products: Product[];
  formModel: Product;

  constructor(private route: ActivatedRoute, private productsService: ProductsService) {}

  onDrop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.products, event.previousIndex, event.currentIndex);
  }

  private getProducts(): void {
    this.products = this.productsService.getProductsByStoreId(this.storeId);
  }

  handleEdit(productId) {
    this.productsService.getProduct(productId)
    .subscribe(product => {
      this.formModel = new Product(product.id, product.name, product.description, product.storeId);
    });
  }

  handleFormSubmit = () => {
    this.productsService.add(this.formModel);
    this.formModel = new Product("", "", "", this.storeId);
  }

  ngOnInit() {
    this.storeId = this.route.snapshot.params['id'];
    this.getProducts();
    this.formModel = new Product("", "", "", this.storeId);
    this.productsService.getProductsOb(this.storeId).subscribe(products => this.products = products);
  }

}
