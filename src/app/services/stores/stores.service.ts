import { Injectable } from '@angular/core';
import { Store } from '../../components/stores/stores.component';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoresService {
  private stores: Store[] = [
    {
      id: 'yv60zhtiih8',
      name: "test name",
      workingTimes: "09:00 - 18:00",
      address: "sds"
    }
  ];

  public add(store: Store): void {
    const strIndex = this.stores.findIndex(item => item.id === store.id);
    
    if (strIndex > -1) {
      this.stores[strIndex] = store;
    } else {
      store.id = Math.random().toString(36).replace('0.', '');
      this.stores.unshift(store);
    }
  }

  public getStore(storeId: string): Observable<Store> {
    return of(this.stores.find(item => item.id === storeId));
  }

  public getStores(): Observable<Store[]> {
    return of(this.stores);
  }

  constructor() { }
}
