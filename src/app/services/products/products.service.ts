import { Injectable } from '@angular/core';
import { Observable, of, Subject  } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { InternalFormsSharedModule } from '@angular/forms/src/directives';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private products: Product[] = [
    {
      id: 'yv60zhtiih8',
      name: "test product",
      description: "asdfas dfas dfas df.",
      storeId: "yv60zhtiih8"
    }
  ];

  subject = new Subject<Product[]>();

  public add(product: Product): void {
    const index = this.products.findIndex(item => item.id === product.id);

    if (index > -1) {
      this.products[index] = product;
    } else {
      product.id = Math.random().toString(36).replace('0.', '');
      this.products.unshift(product);
    }

    this.subject.next(this.products);
  }
  getProductsOb (storeId: string) {
    return this.subject.asObservable().pipe( map(res => res.filter(item => item.storeId === storeId)));
  }

  public getProducts(): Observable<Product[]> {
    return of(this.products);
  }

  public getProductsByStoreId(storeId: string): Product[] {
    return this.products.filter(item => item.storeId === storeId);
  }

  public getProduct(productId: string): Observable<Product> {
    return of(this.products.find(item => item.id === productId));
  }

  constructor() { }
}

export class Product {
  constructor(public id: string, public name: string, public description: string, public storeId: string) {}
}
